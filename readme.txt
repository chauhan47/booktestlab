Book test lab assignment

Parent folder contains the backend code in node with express and mysql as db.

labtest contains the client side code.

## DB FILES
db.js contains query to create tables and insert dummy data.
change credential for local server
    host: 'localhost',
    user: 'root',
    password: 'Hello1234',
    database: 'labtest'

## MAIN FILET
-- For backedend server.js
-- For client side index.js contains component

## scripts to run 
"scripts": {
    "start": "node server.js",
    "server": "nodemon server.js",
    "client": "cd .. && npm start --prefix labtest",
    "dev": "concurrently \"npm run server\" \"npm run client\""
  },