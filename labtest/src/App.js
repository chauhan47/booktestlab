import React from "react";
import './App.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import { Route, Switch, useHistory } from 'react-router-dom'
import LoginComponent from './component/login/LoginComponent';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from "./component/navbar/NavBar";
import CartComponent from "./component/cart/CartComponent";
import TestComponent from "./component/tests/TestComponent";
import { TestProvider } from "./component/context/DataContext";

function App() {
  const history = useHistory();

  return (
    <div>
      <TestProvider>
        <div id="modalReceipt"></div>
        <NavBar />
        <Switch>
          <Route exact path="/" component={LoginComponent} />
          <Route exact path="/test" component={TestComponent} />
          <Route exact path="/cart" component={CartComponent} />
        </Switch>
      </TestProvider>
    </div>
  );
}

export default App;
