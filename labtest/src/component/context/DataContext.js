// 
// Context for count test lab stats
//


import React, { useState } from "react";

const TestContext = React.createContext();

const TestProvider = props => {
  const [cartCount, setCartCount] = useState(null);
  const [totalAmt, setTotalAmt] = useState(null);
  const [testList, setTestlist] = useState([]);
  const [isCart, setIsCart] = useState(false);

  return (
    <TestContext.Provider
      value={{
        count: [cartCount,setCartCount], 
        amt: [totalAmt, setTotalAmt],
        testList: [testList, setTestlist],
        isCart: [isCart, setIsCart]
      }}
    >
      {props.children}
    </TestContext.Provider>
  );
};


export { TestContext, TestProvider };