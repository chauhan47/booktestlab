import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { TestContext } from "../context/DataContext";
import "./Navbar.css";

function NavBar() {
    const { count, amt } = useContext(TestContext);
    const [cartCount, setCartCount] = count;
    const [totalAmt, setTotalAmt] = amt;
    const history = useHistory();

    const logout = () => {
        localStorage.clear();
        setCartCount(null);
        setTotalAmt(null);
        history.push('/')
    }
    return (
        <nav className="navbar navbar-icon-top navbar-expand navbar-dark bg-dark">
            <a className="navbar-brand" href="#">Pathology</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <Link to="/test" className="nav-link" href="#">
                            <i className="fa fa-home"></i>
                            <span className="sr-only">(current)</span>
                        </Link>
                    </li>
                </ul>
                <ul className="navbar-nav ">
                    <li className="nav-item">
                        <Link to="/cart" className="nav-link" href="#">
                            <i className="fa fa-cart-plus">
                                <span className="badge badge-success">{cartCount}</span>
                            </i>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#" onClick={logout}>
                            <i className="fa fa-sign-out">
                            </i>
                        </a>
                    </li>
                </ul>
                {/* <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form> */}
            </div>
        </nav>

    )
}


export default NavBar;