import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { TestContext } from '../context/DataContext';
import { postReq } from '../services/service';

function TestItem(props) {
    const { test } = props;
    const history = useHistory();
    const [testCount, setTestCount] = useState(test.testCount == null ? 0 : test.testCount);
    const { count, amt } = useContext(TestContext);
    const [cartCount, setCartCount] = count;

    const decreament = async () => {
        if (testCount > 1) {
            let res = await updateCount(testCount - 1)
            if(res){
                setCartCount(cartCount - 1);
                setTestCount(testCount - 1);
            }        }
    }
    const increament = async () => {
        let res = await updateCount(testCount + 1)
        if(res){
            setCartCount(cartCount + 1);
            setTestCount(testCount + 1);
        }
    }
    const addItem = async () => {
        let res = await updateCount(1)
        if(res){
            setCartCount(cartCount - testCount + 1);
            setTestCount(1);
        }
    }
    const removeItem = async () => {
        let res = await updateCount(0)
        if(res){
            setCartCount(cartCount - testCount);
            setTestCount(0);
        }
    }

    const updateCount =async (count) => {
            let url = `http://localhost:5000/cart/update`;
            let data = {
                itemId: test.itemId,
                testCount: count
            }
            let res = await postReq(url, data);
            if(res.status === 200){
                return res
            } else if(res.status === 401 || res.status === 403){
                history.push('/')
            }
    }

    return (
        <div className="row d-flex justify-content-start card-strip">
            <img className="comp-logo mr-4 mb-3"/>
            <div className="info">
                <div className="row px-3 mb-2">
                    <h4 className="dark-text mr-4">{test.itemName}</h4>
                    <p className="mt-1 mr-4 extended-title">{test.itemId}</p>
                </div>
                <div className="row px-3">
                    <p className="mb-1"><span className="fa fa-clock-o"> Test id</span></p>
                    <p className="mb-1"><span className="fa fa-home ml-4">{test.labname}</span></p>
                </div>
                <div className="row px-3">
                    <p><span className=""> {test.keyword}</span></p>
                </div>
                <div className="row px-3 mb-3">
                    Available at
                </div>
                <div className="row px-3">
                    {test.available_at}
                </div>
            </div>
            <div className="v-line ml-auto"></div>
            <div className="price text-start">
                <div className="mb-4">
                    <button className='btn btn-outline-danger btn-sm' onClick={decreament}><span className="fa fa-minus"></span></button>
                    <input id={"total-item-count "+props.key} type="text" name="quant[1]" className="item-count" step="0.01" value={testCount} min="0" max="100" onChange={({ target }) => setTestCount(target.value)} />
                    <button className='btn btn-outline-success btn-sm' onClick={increament}><span className="fa fa-plus"></span></button>
                </div>
                <p className="mb-0">Price</p>
                <div className="row px-3">
                    <h4 className="blue-text mr-2">R$ {test.minPrice}</h4>
                    <p className="mt-1 price-fall mr-5">R$ {testCount * test.minPrice}</p>
                </div>
                {
                    props.isCart ?
                        <div className="col-2 col-  sm-2 col-md-2 text-right">
                            <button type="button" className="btn btn-outline-danger btn-xs" onClick={removeItem}>
                                <i className="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </div> :
                        <div className="btn btn-orange mt-4" onClick={addItem}>Add to Cart</div>
                }
            </div>
        </div>
    )
}

export default TestItem;