import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from "react-router-dom";
import { getReq } from '../services/service';
import TestItem from "./TestItem";
import SearchComponent from "../search/SearchComponent";
import './TestStyle.css'
import { TestContext } from '../context/DataContext';
import Buttons from '../cart/Buttons';

function TestListing(props) {
    const [filterlist, setFilterlist] = useState([]);
    const history = useHistory();
    const {count, amt, testList} = useContext(TestContext);
    const [cartCount, setCartCount] = count;
    const [totalAmt, setTotalAmt] = amt;
    const [testlist, setTestlist] = testList;

// Common component for listing test items
// Fetch data for cart and all test list

    useEffect(() => {
        async function fetchData() {
            // Get list of test
            let url = `http://localhost:5000/${props.url}`;
            let res = await getReq(url);
            if(res.status===401 || res.status===403 ){
                return history.push('/')
            }
// Update cart count
            setCartCount(res.data.reduce(function (r, a) {
                return r + a.testCount;
            }, 0));
// Update total amount
            setTotalAmt(res.data.reduce(function (r, a) {
                return r + (a.testCount*a.minPrice);
            }, 0));
            setTestlist(res.data)
            setFilterlist(res.data)
        }
        fetchData();
    }, []);

// filter record based on the search keyword
    const filterRecords= (val)=>{
        setFilterlist(testlist.filter(test=>
            (test.keyword.indexOf(val) > -1 || test.itemName.indexOf(val) > -1 )
        ))
    }
    return (
        <div className="container-fluid px-1 px-sm-4 py-5 mx-auto text-center">
            <SearchComponent filterRecords={filterRecords} isCart={props.isCart}/>
            {
                filterlist.length === 0  ?
                        <h2>No item Found</h2>
                    :
                    filterlist.map((test, index) => {
                        return (
                            <TestItem key={test.serial} test={test} isCart={props.isCart}/>
                        )
                    })
            }
            {
                props.isCart && filterlist.length ? <Buttons/> : null
            }
        </div>
    )
}

export default TestListing;