import { React } from "react";
import TestListing from "./TestListing";

function TestComponent() {
    return(
        <TestListing url="test" isCart={false}/>
    )
}

export default TestComponent;