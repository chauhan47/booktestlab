import { React, useState } from "react";

function SearchComponent(props) {
    const [searchText, setSearchText] = useState('')
    const handleSubmit=(e) => {
        e.preventDefault();
        props.filterRecords(searchText);
    }
    return (
        <div className="bg-white border rounded mt-2 card-strip">
            <div className="row px-5 mb-3" style={{justifyContent: 'space-between'}}>
            <h5>{props.isCart ? "Your Cart":"All tests"}</h5>
            <form className="form-inline my-2 my-lg-0" onSubmit={handleSubmit}>
                    <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" onChange={({target}) => setSearchText(target.value)} />
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form> 
            </div>
        </div>
    )
}

export default SearchComponent;