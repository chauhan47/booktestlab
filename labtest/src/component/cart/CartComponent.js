import React from 'react';
import '../tests/TestStyle.css'
import TestListing from '../tests/TestListing';
import "./style.css";

function CartComponent() {
    return (
            <TestListing url="cart/getAll" isCart={true}/>
    )
}

export default CartComponent;