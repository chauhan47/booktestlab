import React from 'react';
import Modal from 'react-modal';
import { getFormattedDate } from '../services/commonfunc';

const customStyles = {
    content: {
        top: '35%',
        left: '50%',
        right: 'auto',
        width: '22%',
        minHeight: '550px',
        padding: 0,
        // bottom                : '',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

export default class RecieptModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: this.props.isOpen,
            date: ''
        };
        this.closeModal = this.closeModal.bind(this);
        this.resetAndClose = this.resetAndClose.bind(this);
    }
    componentWillMount() {
        this.setState({ date: getFormattedDate() })
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ modalIsOpen: nextProps.isOpen, date: getFormattedDate() })
    }
    resetAndClose() {
        this.closeModal();
    }
    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    render() {
        const { cartCount, totalAmt, testlist } = this.props;
        return (
            <div>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <div className='modal-header'>
                        <h5>Receipt</h5>
                    </div>
                    <div className='receipt-body'>
                        <b>Sale No__ 002</b>
                        <br />
                        <text>Date: {this.state.date}</text>
                        <table style={{ width: '100%' }}>
                            <tr>
                                <th style={{ width: '5%' }}>#</th>
                                <th style={{ width: '45%' }}>products</th>
                                <th style={{ width: '15%' }}>Quantity</th>
                                <th style={{ width: '35%' }}>Sub Total</th>
                            </tr>
                        </table>
                        {testlist.length !== 0 ?
                            <div>
                                <hr />
                                <table>
                                    {testlist.map((test, index) => {
                                        return (
                                            <tr>
                                                <td style={{ width: '5%' }}>{index + 1}</td>
                                                <td style={{ width: '45%' }}>{test.itemId}</td>
                                                <td style={{ width: '25%' }}>{test.testCount}</td>
                                                <td style={{ width: '25%' }}>{(test.testCount * test.minPrice).toFixed(3)} INR</td>
                                            </tr>
                                        )
                                    })
                                    }
                                </table>
                            </div> : <div />
                        }
                        <hr />
                        <div className="grid-container-receipt">
                            <div ></div>
                            <div >VAT</div>
                            <div ><b>{10}%</b></div>
                        </div>
                        <div className="grid-container-receipt">
                            <div>Total items</div>
                            <div >{cartCount} Total</div>
                            <div style={{ width: '100%', fontStyle: 'bold' }}><b>{totalAmt + 1.1} INR</b></div>
                        </div>
                        <hr />
                        <br />
                    </div>
                    <hr />
                    <div style={{ padding: '30px' }}>
                        <button className='close-btn' onClick={this.resetAndClose}>Close</button>
                    </div>
                </Modal>
            </div>
        );
    }
}
