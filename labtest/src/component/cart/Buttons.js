import React, { useContext } from 'react';
import ReactDOM from 'react-dom';
import { TestContext } from '../context/DataContext';
import { getReq} from '../services/service';
import RecieptModal from './RecieptModal';
export default function Buttons(props) {
    const {count, amt, testList} = useContext(TestContext);
    const [cartCount, setCartCount] = count;
    const [totalAmt, setTotalAmt] = amt;
    const [testlist, setTestlist] = testList;

    const modalReceipt = () => {
        ReactDOM.render(
            <RecieptModal isOpen={true}  totalAmt={totalAmt} cartCount={cartCount} testlist={testlist}/>,
            document.getElementById('modalReceipt')
        )
    }
    const cancelSale = async() => {
        let url = `http://localhost:5000/cart/cancel`;
        let res = await getReq(url);
        if(res.status === 200){
            setCartCount(0);
            setTotalAmt(0.00);
        }
    }
    return (
        <div className="btns-bg card-strip text-right">
            {/* <button className='cancel-btns' onClick={cancelSale}>CANCEL SALE</button> */}
            <button className='process-btns' onClick={modalReceipt}>PROCESS SALE</button>
        </div>
    )

}