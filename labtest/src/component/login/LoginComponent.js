import axios from 'axios';
import React, {useState} from 'react';
import { useHistory } from 'react-router-dom';
import './LoginStyle.css'

function LoginComponent() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();

// Handle submi to login 

    const handleSubmit = async (e) => {
        e.preventDefault();
        let url = `http://localhost:5000/user/login`;
        let data = {
            email: email,
            password: password
        }
        let res = await axios.post(url, data);
// Save token to localstorage
// Redirect to Test list page
        if(res.status === 200){
            let data = JSON.stringify(res.data);
            localStorage.setItem('userToken', data);
            history.push('/test')
        }
      }
    return (
        <div className="container-fluid px-1 px-sm-4 py-5 mx-auto bg" > 
            <div id="login-row" className="row justify-content-end align-items-end">
                <div id="login-column" className="col-md-50">
                    <div className="box">
                        <p className="login-msg-above"> <h2>Annotation Tool Login</h2></p>
                        <br />
                        <div className="shape1"></div>
                        <div className="shape2"></div>
                        <div className="shape3"></div>
                        <div className="shape4"></div>
                        <div className="shape5"></div>  
                        <div className="shape6"></div>
                        <div className="shape7"></div>
                        <div className="float">
                            <form className="form" onSubmit={handleSubmit} action="">
                                <div className="form-group">
                                    <label for="email" className="text-white">email:</label><br />
                                    <input type="text" name="email" id="email" className="form-control" defaultValue={email} onChange={({target}) => setEmail(target.value)}/>
                                </div>
                                <div className="form-group">
                                    <label for="password" className="text-white">Password:</label><br />
                                    <input type="text" name="password" id="password" className="form-control" defaultValue={password} onChange={({target}) => setPassword(target.value)}/>
                                </div>
                                <div className="form-group">
                                    <input type="submit" name="submit" className="btn btn-info btn-md" value="submit"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginComponent;