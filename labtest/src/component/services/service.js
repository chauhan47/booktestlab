import axios from "axios";
import { getToken } from "./commonfunc";

let tokenStr = "";
let authpost = "";
let authget = "";

const headers = () => {
    tokenStr = getToken();
    authpost = {'Content-Type': 'application/json', "authorization": `Bearer ${tokenStr}` }
    authget = {"authorization": `Bearer ${tokenStr}` }
}

const postReq = async (url, data) => {
    headers()
    const rawResponse = await axios.post(url, data, {
        headers: authpost
    })
    .catch(function(err){
        return err.response
    })
    return rawResponse; 
}

const getReq = async (url) => {
    headers()
    const rawResponse = await axios.get(url, {
        headers: authget
    })
    .catch(function(err){
        return err.response
    })
    return rawResponse;
}

export { postReq, getReq };