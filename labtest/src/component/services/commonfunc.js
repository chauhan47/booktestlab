function getFormattedDate(){
    let date = new Date();
    let day = date.toISOString().split('T')[0]
    day = `${day.split('-')[2]}-${day.split('-')[1]}-${day.split('-')[0]}`
    let time = date.toTimeString().split(' ')[0]

    return `${day} ${time}`
}

function loggedIn() {
    const userInfo = JSON.parse(localStorage.getItem('userToken'));
    if(userInfo){
        return true;
    } else 
    return false
}

function getToken() {
    const userInfo = JSON.parse(localStorage.getItem('userToken'));
    if(userInfo){
        return userInfo.token;
    } else 
    return false
}

export {getFormattedDate, getToken, loggedIn}