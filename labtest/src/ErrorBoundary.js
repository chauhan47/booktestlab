import React, {Component} from 'react';
import ErrorService from './component/services/ErrorService';

export default class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch(error, info) {
        this.setState({ hasError: true });
        ErrorService.logErrorToMyService(error, info);
    }

    render() {
        if (this.state.hasError) {
            return
            <div>
                <h1>Something went wrong.</h1>;
                <h1>Something went wrong.</h1>;
                <h1>Something went wrong.</h1>;
            </div>
        }
        return this.props.children;
    }
}