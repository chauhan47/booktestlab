var express = require('express');
var router = express.Router();
const authController = require('../controllers/auth')

/* register user. */
router.post('/register', authController.register);

// Login user
router.post('/login', authController.login);

module.exports = router;
