const express = require('express');
const router = express.Router();
const testController = require('../controllers/test')
const {authUser} = require('../controllers/auth')

/* GET users listing. */
router.get('/', authUser, testController.getAllTests);

module.exports = router;
