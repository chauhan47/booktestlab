const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cart')
const {authUser} = require('../controllers/auth')

/* GET users listing. */
router.post('/update', authUser, cartController.addToCart);

router.get('/getAll', authUser, cartController.getCartItems);

module.exports = router;
