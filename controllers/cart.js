const db = require('../src/database/connection');

exports.addToCart = (req, res) => {
    try {
        const { itemId, testCount } = req.body;
        const user_id = req.user.id;

        db.query('SELECT * FROM bookedtests WHERE user_id = ? and itemId = ?', [user_id, itemId], (err, result) => {
            if (result.length > 0) {
                let b_id = result[0].booking_id;
                console.log("res", testCount, b_id);
                if (testCount == 0) {
                    db.query('DELETE FROM bookedtests WHERE booking_id = ?', [b_id], (err, result) => {
                        return res.status(200).json({ msg: 'Item removed from cart' })
                    })
                } else
                    db.query('UPDATE bookedtests SET testCount = ? WHERE booking_id = ?', [testCount, b_id], (err, result) => {
                        return res.status(200).json({ msg: 'Item updated in cart' })
                    })
            } else {
                let data = {user_id, itemId, testCount}
                console.log(data)
                db.query('INSERT INTO bookedtests SET  ?', data, (err, result) => {
                    return res.status(200).json({ msg: 'Item Added in cart' })
                })
            }
        })
    } catch (error) {
        throw error;
    }
}

exports.getCartItems = (req, res) => {
    let sql = `select t.serial, t.itemId, t.itemName, t.type, t.keyword, t.best_sellers,COALESCE(b.testCount, 0) as testCount, t.included_tests,t.url,
    t.minPrice, t.labName, t.fasting, t.available_at, t.popular, t.category from test t  join  bookedtests b 
    on t.itemId = b.itemId`
       try {
           db.query(sql, (err, result) => {
               if(err) throw err;
               return res.status(200).json(result)
           })
       } catch (err) {
           throw err;
       }
}