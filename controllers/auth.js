const db = require('../src/database/connection');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')


// service for regiter
exports.register = async (req, res) => {
    const { name, email, password } = req.body;

    let hashedPass = await bcrypt.hash(password, 8);
    let record = {
        name: name,
        email: email,
        password: hashedPass
    }

    db.query('INSERT INTO users SET ? ', record , (err, result) => {
        if(err) throw err;
        res.res('User created')
    })
}

// Service for login
exports.login = async (req, res) => {

    console.log("reached");
    try {
        const { email, password } = req.body;
        if(!email || !password){
            return res.status(400).json({})
        }
        
        db.query('SELECT * FROM users WHERE email = ? ', [email] ,async (err, result) => {
            if(err) throw err;
            if(result.length == 0){
                return res.status(401).json({msg: 'User not found'})
            }
            if(password != result[0].password){//!(await bcrypt.compare(password, result[0].password))){
                return res.status(401).json({})
            } else {
                const id = result[0].user_id;
                const token = jwt.sign({id}, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_EXPIRESIN
                })

                // const cookieOptions = {
                //     expires: new Date(
                //         Date.now() + process.env.JWT_COOKIE_EXPIRESIN *24 *60*60*1000
                //     ),
                //     httpOnly: true
                // }
                // res.cookie('jwt', token, cookieOptions);
                return res.status(200).json({token})
            }
        })
    } catch (error) {
        console.log(error)
    }
}

// Service for authenticating token
exports.authUser = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.status(401).json({msg: 'Invalid token'})

    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
        if(err) return res.status(403).json({msg: 'Token expired'})

        req.user = user;
        next()
    })
}