
// create database 
// create table test, users, bookedtests
// add data to test table


const mysql = require('mysql');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Hello1234',
    database: 'labtest'
})

db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('sql connected');

    let sql = 'create database labtest';

    let testsql = `create table test(
        serial int not null,
        itemId varchar(40) not null,
        itemName varchar(255) not null,
        type varchar(20),
        keyword varchar(255),
        best_sellers varchar(255),
        testCount int,
        included_tests varchar(100),
        url varchar(255),
        minprice varchar(10),
        labname varchar(100),
        fasting int,
        available_at int,
        popular boolean,
        category varchar(40)
    )`;
    
    let usersql = `create table users(user_id int primary key, name varchar(40) not null, email varchar(30) not null, password varchar(255) not null)`;
    let cartsql = `create table bookedtests(booking_id int primary key, user_id varchar(40) not null, itemId varchar(40), testCount int, paid boolean, tested_status varchar(10) )`;
    let adddata = `insert into test set ?`;
    let testTableData = [
        {
          "serial": 19956,
          "itemId": "DIANM11",
          "itemName": "COVID-19 Test",
          "type": "Test",
          "Keyword": "covid-19-test",
          "best_sellers": "",
          "testCount": 1,
          "included_tests": "",
          "url": "covid-19-test",
          "minPrice": 4500,
          "labName": "Metropolis",
          "fasting": 0,
          "available_at": 1,
          "popular": true,
          "category": "path"
        },
        {
          "serial": 1995,
          "itemId": "DIA2044",
          "itemName": "Eye Test- Vision Express",
          "type": "Test",
          "Keyword": "eye,test",
          "best_sellers": "",
          "testCount": 1,
          "included_tests": "",
          "url": "eye_test",
          "minPrice": 49,
          "labName": "Vision Express",
          "fasting": 0,
          "available_at": 1,
          "popular": true,
          "category": "path"
        },
        {
          "serial": 1983,
          "itemId": "DIAR894",
          "itemName": "Yttrium Therapy",
          "type": "Test",
          "Keyword": "Yttrium,Therapy",
          "testCount": 1,
          "included_tests": "",
          "url": "Yttrium-Therapy-test-cost",
          "minPrice": 17500,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1982,
          "itemId": "DIAR893",
          "itemName": "X Ray Wrist Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Wrist,Lateral,View",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Wrist-Lateral-View-test-cost",
          "minPrice": 120,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1981,
          "itemId": "DIAR892",
          "itemName": "X Ray Wrist AP View",
          "type": "Test",
          "Keyword": "X,Ray,Wrist,AP,View",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Wrist-AP-View-test-cost",
          "minPrice": 120,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1980,
          "itemId": "DIAR891",
          "itemName": "X Ray Wrist AP and Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Wrist,AP,and",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Wrist-AP-and-Lateral-View-test-cost",
          "minPrice": 240,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1979,
          "itemId": "DIAR890",
          "itemName": "X Ray Whole Spine Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Whole,Spine,Lateral",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Whole-Spine-Lateral-View-test-cost",
          "minPrice": 320,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1978,
          "itemId": "DIAR889",
          "itemName": "X Ray Whole Spine Lateral and AP View",
          "type": "Test",
          "Keyword": "X,Ray,Whole,Spine,Lateral",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Whole-Spine-Lateral-and-AP-View-test-cost",
          "minPrice": 560,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1977,
          "itemId": "DIAR888",
          "itemName": "X Ray Whole Spine AP View",
          "type": "Test",
          "Keyword": "X,Ray,Whole,Spine,AP",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Whole-Spine-AP-View-test-cost",
          "minPrice": 320,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1976,
          "itemId": "DIAR887",
          "itemName": "X Ray Water View",
          "type": "Test",
          "Keyword": "X,Ray,Water,View,",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Water-View-test-cost",
          "minPrice": 145,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1975,
          "itemId": "DIAR886",
          "itemName": "X Ray Tm Joint Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Tm,Joint,Lateral",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Tm-Joint-Lateral-View-test-cost",
          "minPrice": 162,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1974,
          "itemId": "DIAR885",
          "itemName": "X Ray Tm Joint AP View",
          "type": "Test",
          "Keyword": "X,Ray,Tm,Joint,AP",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Tm-Joint-AP-View-test-cost",
          "minPrice": 162,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1973,
          "itemId": "DIAR884",
          "itemName": "X Ray Tm Joint AP and Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Tm,Joint,AP",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Tm-Joint-AP-and-Lateral-View-test-cost",
          "minPrice": 280,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1972,
          "itemId": "DIAR883",
          "itemName": "X Ray Thumb Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Thumb,Lateral,View",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Thumb-Lateral-View-test-cost",
          "minPrice": 120,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1971,
          "itemId": "DIAR882",
          "itemName": "X Ray Thumb Lateral and AP View",
          "type": "Test",
          "Keyword": "X,Ray,Thumb,Lateral,and",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Thumb-Lateral-and-AP-View-test-cost",
          "minPrice": 240,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1970,
          "itemId": "DIAR881",
          "itemName": "X Ray Thumb AP View",
          "type": "Test",
          "Keyword": "X,Ray,Thumb,AP,View",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Thumb-AP-View-test-cost",
          "minPrice": 120,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1969,
          "itemId": "DIAR880",
          "itemName": "X Ray Thigh Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Thigh,Lateral,View",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Thigh-Lateral-View-test-cost",
          "minPrice": 120,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1968,
          "itemId": "DIAR879",
          "itemName": "X Ray Thigh AP View",
          "type": "Test",
          "Keyword": "X,Ray,Thigh,AP,View",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Thigh-AP-View-test-cost",
          "minPrice": 120,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1967,
          "itemId": "DIAR878",
          "itemName": "X Ray Thigh AP and Lateral View",
          "type": "Test",
          "Keyword": "X,Ray,Thigh,AP,and",
          "testCount": 1,
          "included_tests": "",
          "url": "X-Ray-Thigh-AP-and-Lateral-View-test-cost",
          "minPrice": 240,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1966,
          "itemId": "DIAR877",
          "itemName": "X ray Temp",
          "type": "Test",
          "Keyword": "X,ray,Temp",
          "testCount": 1,
          "included_tests": "",
          "url": "X-ray-Temp-test-cost",
          "minPrice": 0,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 1998,
          "itemId": "DIAR877",
          "itemName": "X ray Temp",
          "type": "Test",
          "Keyword": "X,ray,Temp",
          "testCount": 1,
          "included_tests": "",
          "url": "X-ray-Temp-test-cost",
          "minPrice": 0,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 4455,
          "itemId": "DIAR877",
          "itemName": "X ray Temp",
          "type": "Test",
          "Keyword": "X,ray,Temp",
          "testCount": 1,
          "included_tests": "",
          "url": "X-ray-Temp-test-cost",
          "minPrice": 0,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 8787,
          "itemId": "DIAR877",
          "itemName": "X ray Temp",
          "type": "Test",
          "Keyword": "X,ray,Temp",
          "testCount": 1,
          "included_tests": "",
          "url": "X-ray-Temp-test-cost",
          "minPrice": 0,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        },
        {
          "serial": 12349,
          "itemId": "DIAR877",
          "itemName": "X ray Temp",
          "type": "Test",
          "Keyword": "X,ray,Temp",
          "testCount": 1,
          "included_tests": "",
          "url": "X-ray-Temp-test-cost",
          "minPrice": 0,
          "labName": "",
          "fasting": 0,
          "available_at": 2,
          "popular": false,
          "category": "radio"
        }
      ]

    db.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);

        sql = 'use labtest';
        db.query(sql, (err, result) => {
            if (err) throw err;

            db.query(testsql, (err, result) => {
                if (err) throw err;
                console.log(result);

                for(let i=0; i<testTableData.length-1; i++){
                  db.query(adddata, testTableData[i], (err, result) => {
                    if (err) throw err;
                    
                    db.query(usersql, (err, result) => {
                      if (err) throw err;
                      
                      db.query(cartsql, (err, result) => {
                        if (err) throw err;
                        console.log(result);
                      })
                    })
                })
                }

            })
        })

    })

})