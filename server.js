const express = require('express');
// for keeping env variables
require('dotenv').config();
const cookieParser = require('cookie-parser')
//My sql connection
const db = require('./src/database/connection')

const app = express();

// Routers
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var cartRouter = require('./routes/cart')

app.use(express.urlencoded({
    extended: true
}))
app.use(cookieParser())
app.use(express.json());

// Handling CORS
app.use(function (req, res, next) {
    // console.log(req);
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With,content-type, Accept, Authorization');
    if(req.method == 'OPTIONS'){
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        return res.status(201).json({});
    }
    next();
})

// Routes
app.use('/test', indexRouter);
app.use('/user', usersRouter);
app.use('/cart', cartRouter);

let port = process.env.PORT;

app.listen(port || 5000, () => {
    console.log(`server started at port ${port}`)
})