const mysql = require('mysql');
require('dotenv').config();

const db = mysql.createConnection({
    host:       'localhost',
    user:       process.env.DBUSER,
    password:   process.env.DBPASS,
    database:   process.env.DBNAME,
    insecureAuth : true
})

db.connect((err)=>{
    if(err){
        throw err;
    }
    console.log('sql connected');
})

module.exports = db;